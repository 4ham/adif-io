#!rspec
require 'spec_helper'

describe ADIF_IO do
  it 'has a version number' do
    expect(ADIF_IO::VERSION).not_to be nil
  end

  context "ADI" do
    it 'can parse adif from spec' do
      input = "<call:6>WN4AZY<band:3>20M<mode:4>RTTY<qso_date:8>19960513<time_on:4>1305<eor>"
      adif = ADIF_IO.parse_adi(input).records
      expect(adif.size).to be 1
      record = adif[0]

      expect(record["call"]).to eq 'WN4AZY'
      expect(record["band"]).to eq '20m'
      expect(record["mode"]).to eq 'RTTY'
      expect(record["qso_date"]).to eq '19960513'
      expect(record["time_on"]).to eq '1305'
      expect(record.raw).to eq input
    end

    it 'must ignore cases' do
      input = "<CALL:6>WN4AZY<BAND:3>20m<MoDe:4>RTtY<qso_DATe:8>19960513<TIME_ON:4>1305<EoR>"
      adif = ADIF_IO.parse_adi(input).records
      expect(adif.size).to be 1
      record = adif[0]

      expect(record["call"]).to eq 'WN4AZY'
      expect(record["band"]).to eq '20m'
      expect(record["mode"]).to eq 'RTTY'
      expect(record["qso_date"]).to eq '19960513'
      expect(record["time_on"]).to eq '1305'
      expect(record.raw).to eq input
    end

    it 'must ignore garbage after data' do
      input = "<call:6>WN4AZY
      <band:3>20M
      <mode:4>RTTY
      <qso_date:8>19960513
      <time_on:4>1305\n\n
      <eor>"
      adif = ADIF_IO.parse_adi(" <eoh>" + input + "\n\n\n")
      expect(adif.records.size).to be 1
      record = adif.records[0]

      expect(record["call"]).to eq 'WN4AZY'
      expect(record["band"]).to eq '20m'
      expect(record["mode"]).to eq 'RTTY'
      expect(record["qso_date"]).to eq '19960513'
      expect(record["time_on"]).to eq '1305'
      expect(record.raw).to eq input
    end

    it 'can parse multiple records' do
      input = "
        <call:6>WN4AZY<band:3>20M<mode:4>RTTY
        <qso_date:8:d>19960513<time_on:4>1305<eor>
        
        <call:5>N6MRQ<band:2>2M<mode:2>fM
        <qso_date:8:d>19961231<time_on:6>235959<eor>"
      adif = ADIF_IO.parse_adi("Blub blab blob<EoH>" + input + "\ntrailing junk\n\n").records
      expect(adif.size).to be 2
      
      record0 = adif[0]
      expect(record0["call"]).to eq 'WN4AZY'
      expect(record0["band"]).to eq '20m'
      expect(record0["mode"]).to eq 'RTTY'
      expect(record0["qso_date"]).to eq '19960513'
      expect(record0["time_on"]).to eq '1305'

      record1 = adif[1]
      expect(record1["call"]).to eq 'N6MRQ'
      expect(record1["band"]).to eq '2m'
      expect(record1["mode"]).to eq 'FM'
      expect(record1["qso_date"]).to eq '19961231'
      expect(record1["time_on"]).to eq '235959'
      expect(record0.raw + record1.raw).to eq input
    end

    it 'can allow header' do
      header_input = "
        this data was exported using WF1B RTTY version 9, conforming to ADIF standard specification version 9.99
        <eoh>"        
      input = header_input + "
        <call:6>wn4AZY
        <band:3>20m
        <mode:4>rTTy
        <qso_date:8>19960513
        <time_on:4>1305
        <eor>"
      adif = ADIF_IO.parse_adi(input)
      expect(adif.records.size).to be 1
      expect(adif.header.raw).to eq header_input
      record = adif.records[0]

      expect(record["call"]).to eq 'WN4AZY'
      expect(record["band"]).to eq '20m'
      expect(record["mode"]).to eq 'RTTY'
      expect(record["qso_date"]).to eq '19960513'
      expect(record["time_on"]).to eq '1305'

      adif = ADIF_IO.parse_adi(<<-EOS)
            Generated on 2011-11-22 at 02:15:23Z for WN4AZY

            <adif_ver:5>3.0.4
            <programid:7>MonoLog
            <USERDEF1:8:N>QRP_ARCI
            <USERDEF2:19:E>SweaterSize,{S,M,L}

            <USERDEF3:15:N>ShoeSize,{5:20}
            <EOH>

            <call:6>WN4AZY
            <band:3>20M
            <mode:4>RTTY
            <qso_date:8>19960513
            <qrp_arci:4>1234
            <SweaterSize:1>M
            <ShoeSize:2>17
            <time_on:4>1305
            <eor>

            <call:6>WN4AZZ
            <band:3>40M
            <mode:2>CW
            <qso_date:8>19960513
            <time_on:4>1305
            <qrp_arci:5>Lorem
            <SweaterSize:1>Y
            <ShoeSize:1>4
            <eor>
            EOS

      header = adif.header
      expect(header["adif_ver"]).to eq '3.0.4'
      expect(header["programid"]).to eq 'MonoLog'
      expect(header["userdef1"]).to eq 'QRP_ARCI'
      expect(header["userdef2"]).to eq 'SweaterSize,{S,M,L}'
      expect(header["userdef3"]).to eq 'ShoeSize,{5:20}'

      adif = adif.records
      expect(adif.size).to be 2
      record_good = adif[0]
      expect(record_good["call"]).to eq 'WN4AZY'
      expect(record_good["band"]).to eq '20m'
      expect(record_good["mode"]).to eq 'RTTY'
      expect(record_good["qso_date"]).to eq '19960513'
      expect(record_good["time_on"]).to eq '1305'
      expect(record_good["qrp_arci"]).to eq "1234"
      expect(record_good["sweatersize"]).to eq "M"
      expect(record_good["shoesize"]).to eq "17"
      puts record_good.broken_fields_import.inspect
      expect(record_good.adif_ok_lenient?).to be_truthy
      expect(record_good.adif_ok?).to be_truthy
      expect(record_good.broken_fields_import).to be_empty
      expect(record_good.broken_fields_export).to be_empty

      record_bad = adif[1]
      expect(record_bad["call"]).to eq 'WN4AZZ'
      expect(record_bad["band"]).to eq '40m'
      expect(record_bad["mode"]).to eq 'CW'
      expect(record_bad["qso_date"]).to eq '19960513'
      expect(record_bad["time_on"]).to eq '1305'
      expect(record_bad["qrp_arci"]).to eq "Lorem"
      expect(record_bad["sweatersize"]).to eq "Y"
      expect(record_bad["shoesize"]).to eq "4"
      expect(record_bad.broken_fields_import).to contain_exactly("qrp_arci","sweatersize", "shoesize")
      expect(record_bad.broken_fields_export).to contain_exactly("qrp_arci","sweatersize", "shoesize")
      expect(record_bad.adif_ok_lenient?).to be_falsy
      expect(record_bad.adif_ok?).to be_falsy
      
    end

    it 'does not include invalid row' do
      expect(ADIF_IO.parse_adi("<CALL:6>WN4AZY").records.size).to be 0
    end

    it 'keeps case of strings' do
      comment = "Lorem? Ipsum!"
      expect(ADIF_IO.parse_adi("<Comment:#{comment.length}>#{comment}<eoR>").records[0]["comment"]).to eq comment
    end

    it 'translates calls to upper case' do
      call = "Dj3Ei"
      expect(ADIF_IO.parse_adi("<CAll:#{call.length}>#{call}<eoR>").records[0]["call"]).to eq call.upcase
    end

    it 'has datetime_on/datetime_off method for utility' do
      adif = ADIF_IO.parse_adi(<<-EOS).records
            Lorem? Ipsum!
            <eoh>
            <qso_date:8:d>19960513<time_on:4>1305
            <qso_date_off:8:d>19960513<time_off:4>1310<eor>
            <qso_date:8:d>19961231<time_on:6>235959
            <qso_date_off:8:d>19970101<time_off:6>000005<eor>
            EOS

      record = adif[0]
      expect(record.datetime_on).to be_instance_of(DateTime)
      expect(record.datetime_on.strftime('%Y-%m-%d %H:%M:%S')).to eq "1996-05-13 13:05:00"
      expect(record.datetime_off).to be_instance_of(DateTime)
      expect(record.datetime_off.strftime('%Y-%m-%d %H:%M:%S')).to eq "1996-05-13 13:10:00"

      record = adif[1]
      expect(record.datetime_on).to be_instance_of(DateTime)
      expect(record.datetime_on.strftime('%Y-%m-%d %H:%M:%S')).to eq "1996-12-31 23:59:59"
      expect(record.datetime_off).to be_instance_of(DateTime)
      expect(record.datetime_off.strftime('%Y-%m-%d %H:%M:%S')).to eq "1997-01-01 00:00:05"
    end

    it 'rejects fields not in the ADIF spec' do
      input = "<qso_date:8:d>20190811<no_such_Field:4>Junk<EoR>"
      adif = ADIF_IO.parse_adi("Don't care what I mumble here.\n\n<EoH>#{input}\n\n\n").records
      expect(adif.size).to be 1
      record = adif[0]
      expect(record["qso_date"]).to eq '20190811'
      expect(record["no_such_field"]).to eq 'Junk'
      expect(record.raw).to eq input
      expect(record.adif_ok?).to be_falsy
      expect(record.adif_ok_lenient?).to be_falsy
      expect(record.field_ok_for_output?("qso_date")).to be_truthy
      expect(record.field_ok_for_output?("no_such_field")).to be_falsy
      expect(record.field_ok_for_input?("qso_date")).to be_truthy
      expect(record.field_ok_for_input?("no_such_field")).to be_falsy
      expect(record.broken_fields_import).to contain_exactly("no_such_field")
      expect(record.broken_fields_export).to contain_exactly("no_such_field")
    end

    # Nice idea, but ADIF never deprecates a field, but only an enumeration.
    # it 'marks deprecated fields' do
    # end

    it 'doesn\'t pretend to know external enumerations' do
      r1 = "<DARC_dok:10>LoremIpsum<EoR>"
      adif = ADIF_IO.parse_adi(r1).records
      expect(adif.size).to be 1
      r = adif[0]
      expect(r["darc_dok"]).to eq "LOREMIPSUM"
      expect(r.adif_ok?).to be_truthy
      expect(r.adif_ok_lenient?).to be_truthy
    end

    it 'marks deprecated values' do
      r1 = "<qso_date:8>20190811<MoDe:5>Jt65a<EoR>"
      r2 = "<qso_date:8>20190811<mode:4>jt65<EoR>"
      adif = ADIF_IO.parse_adi(r1 + r2).records
      expect(adif.size).to be 2

      expect(adif[0]["qso_date"]).to eq '20190811'
      expect(adif[0]["mode"]).to eq 'JT65A'
      expect(adif[0].raw).to eq r1
      expect(adif[0].adif_ok?).to be_falsy
      expect(adif[0].adif_ok_lenient?).to be_truthy
      expect(adif[0].broken_fields_export).to contain_exactly("mode")
      expect(adif[0].broken_fields_import).to be_empty
      expect(adif[0].field_ok_for_output?("qso_date")).to be_truthy
      expect(adif[0].field_ok_for_output?("mode")).to be_falsy
      
      expect(adif[1]["qso_date"]).to eq '20190811'
      expect(adif[1]["mode"]).to eq 'JT65'
      expect(adif[1].raw).to eq r2
      expect(adif[1].adif_ok?).to be_truthy
      expect(adif[1].adif_ok_lenient?).to be_truthy
      expect(adif[1].broken_fields_import).to be_empty
      expect(adif[1].broken_fields_export).to be_empty
      expect(adif[1].field_ok_for_output?("qso_date")).to be_truthy
      expect(adif[1].field_ok_for_output?("mode")).to be_truthy
    end

    it 'accepts app fields' do
      adif = ADIF_IO.parse_adi("<mode:2>CW<app_Blub_Blab:8>blubblab<eor>").records
      expect(adif.size).to eq 1
      r = adif[0]
      expect(r["app_blub_blab"]).to eq "blubblab"
      expect(r.broken_fields_export).to contain_exactly("app_blub_blab")
      expect(r.broken_fields_import).to be_empty
      expect(r.adif_ok_lenient?).to be_truthy
      expect(r.adif_ok?).to be_falsy
    end
  end

  # context "ADX" do
  #   adif = ADIF_IO.parse_adx(<<-EOS)
  #           <?xml version="1.0" encoding="UTF-8"?>
  #           <ADX>
  #               <HEADER>
  #                   <ADIF_VER>3.0.4</ADIF_VER>
  #                   <PROGRAMID>monolog</PROGRAMID>
  #                   <USERDEF FIELDID="1" TYPE="N">EPC</USERDEF>
  #                   <USERDEF FIELDID="2" TYPE="E" ENUM="{S,M,L}">SWEATERSIZE</USERDEF>
  #                   <USERDEF FIELDID="3" TYPE="N" RANGE="{5:20}">SHOESIZE</USERDEF>
  #               </HEADER>
  #               <RECORDS>
  #                   <RECORD>
  #                       <QSO_DATE>19900620</QSO_DATE>
  #                       <TIME_ON>1523</TIME_ON>
  #                       <CALL>VK9NS</CALL>
  #                       <BAND>20M</BAND>
  #                       <MODE>RTTY</MODE>
  #                       <USERDEF FIELDNAME="SWEATERSIZE">M</USERDEF>
  #                       <USERDEF FIELDNAME="SHOESIZE">11</USERDEF>
  #                       <APP PROGRAMID="MONOLOG" FIELDNAME="Compression" TYPE="s">off</APP>
  #                   </RECORD>
  #                   <RECORD>
  #                       <QSO_DATE>20101022</QSO_DATE>
  #                       <TIME_ON>0111</TIME_ON>
  #                       <CALL>ON4UN</CALL>
  #                       <BAND>40M</BAND>
  #                       <MODE>PSK</MODE>
  #                       <SUBMODE>PSK63</SUBMODE>
  #                       <USERDEF FIELDNAME="EPC">32123</USERDEF>
  #                       <APP PROGRAMID="MONOLOG" FIELDNAME="COMPRESSION" TYPE="s">off</APP>
  #                   </RECORD>
  #               </RECORDS>
  #           </ADX>
  #       EOS
  #   # p adif
  # end
end

describe ADIF_IO::Writer do

  context "adi" do
    it 'writes an adi stream' do
      io = StringIO.new
      writer = ADIF_IO::Writer.new(io, :adi)
            
      writer << ADIF_IO::Header.new(nil, nil, nil, {
                                   :userdef => [
                                        { :type => 'E', :enum => '{S,M,L}' },
                                        'SWEATERSIZE'
                                      ],
                                 })
            
      writer << ADIF_IO::Record.new(nil, nil, nil, {
                                   :call => 'JH1UMV',
                                   :qso_date => '20140624',
                                   :time_on => '230000',
                                 })
      writer.finish

      readback = ADIF_IO.parse_adi(io.string)
      expect(readback.header["programid"]).to eq 'adif-io.rb'
      expect(readback.header["programversion"]).to eq ADIF_IO::VERSION
      expect(readback.header["userdef"]).to eq 'SWEATERSIZE'
      expect(readback.records.size).to be 1
      expect(readback.records[0]["call"]).to eq 'JH1UMV'
    end

    it 'writes an adi header automatically' do
      io = StringIO.new
      writer = ADIF_IO::Writer.new(io, :adi)
      writer << ADIF_IO::Record.new(nil, nil, nil, {
                                   :call => 'JH1UMV',
                                   :qso_date => '20140624',
                                   :time_on => '230000',
                                 })
            
      writer.finish

      readback = ADIF_IO.parse_adi(io.string)
      expect(readback.header["programid"]).to eq 'adif-io.rb'
      expect(readback.header["programversion"]).to eq ADIF_IO::VERSION
      expect(readback.header["adif_ver"]).to eq "3.1.0"
      expect(readback.records.size).to be 1
      expect(readback.records[0]["call"]).to eq 'JH1UMV'
    end

    it 'writes the requested header when asked' do
      io = StringIO.new
      writer = ADIF_IO::Writer.new(io, :adi)
      writer << ADIF_IO::Header.new(nil, nil, nil)
      writer << ADIF_IO::Record.new(nil, nil, nil, {
                                   :call => 'JH1UMV',
                                   :qso_date => '20140624',
                                   :time_on => '230000',
                                 })
            
      writer.finish

      readback = ADIF_IO.parse_adi(io.string)
      expect(readback.header["programid"]).to eq 'adif-io.rb'
      expect(readback.header["programversion"]).to eq ADIF_IO::VERSION
      expect(readback.records.size).to be 1
      expect(readback.records[0]["call"]).to eq 'JH1UMV'
    end
  end

  # context "adx" do
  #   # This has not been maintained for a while.
  #   it 'writes an adx stream' do
  #     io = StringIO.new
  #     writer = ADIF::Writer.new(io, :adx)
  #           
  #     writer << ADIF::Header.new({
  #                                  :programid => 'ruby-adif',
  #                                  :userdef   => [{ :type => 'E', :enum => '{S,M,L}' }, 'SWEATERSIZE'],
  #                                })
  #           
  #     writer << ADIF::Record.new({
  #                                  :call => 'JH1UMV',
  #                                  :qso_date => '20140624',
  #                                  :time_on => '230000',
  #                                })
  #           
  #     writer.finish
  # 
  #     # This should work, but does not presently.
  #     # It is expected https://github.com/cho45/adif/issues/2 will fix this.
  #     # readback = ADIF_IO.parse_adx(io.string)
  #   end
  # end
end
