# ADIF

This is an ADIF (Amateur Data Interchange Format) parser and writer
library for Ruby.

ADIF is an open standard for storage and exchange of QSO data, which
is something ham radio amateurs are interested in.

You may also be interested in the YAML file that describes the ADIF
format, available at [adif.yaml](adif.yaml).

## Installation

This gem is not (yet) available via
[rubygems.org](https://rubygems.org), but you can easily build it
yourself:

    git clone https://gitlab.com/4ham/adif-io
    cd adif-io
    gem build adif-io.gemspec
    gem install adif-io-0.5.0.gem

To use it, add this line to your application's Gemfile:

    gem 'adif-io', '~> 0.5.0'

And then execute:

    bundle

## Usage

Not well documented yet, but you are invited to look into the tests at
[./spec/adif_spec.rb](./spec/adif_spec.rb )

## Contributing

* Have Ruby installed,
* make sure you have the `bundle` gem installed with `gem install bundle`
* fork https://gitlab.com/andreas_krueger_rb/adif-io and clone your fork,
* pull in rspec with `bundle install` in the directory of your fork
  that contains this README.md file
* create your feature branch (`git checkout -b my-new-feature`),
* change and test (`rspec spec/adif-io_spec.rb`),
* This contains generated code. If you need to change that generated code,
  run `bundle install` in the `build_code` directory and run `rake` (if
  you changed anything down there).
* commit your changes (`git commit -am 'Add some feature'`),
* push to the branch (`git push origin my-new-feature`),
* create a new pull request at https://gitlab.com/andreas_krueger_rb/adif_io.
