# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'adif-io/version'

Gem::Specification.new do |spec|
  spec.name          = "adif-io"
  spec.version       = ADIF_IO::VERSION
  spec.authors       = ["dj3ei"]
  spec.email         = ["dj3ei@famsik.de"]
  spec.summary       = %q{ADIF Parser/Writer/Checker}
  spec.description   = %q{ADIF (Amateur Data Interchange Format) is an open standard for exchange of data between ham radio software.}
  spec.homepage      = "https://gitlab.com/andreas_krueger_rb/adif_io"
  spec.license       = "AGPL-3.0-or-later"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 13.0"
  spec.add_development_dependency "rspec", "~> 3.9"
  spec.add_development_dependency "simplecov", "~> 0.18"
  
end
