require "adif-io/version"
require "date"
require "strscan"
require "rexml/document"
require "import_field_checker"
require "export_field_checker"
require "field_normalizer"

class ADIF_IO

  def self.parse_adi(adif)

    s = StringScanner.new(adif)
    
    import_field_checker = ImportFieldChecker.new
    export_field_checker = ExportFieldChecker.new
    field_normalizer = FieldNormalizer.new

    headers = []
    records = []

    header = Header.new(import_field_checker, export_field_checker, field_normalizer)
    if s.peek(1) == '<'
      feedme = records
      current = Record.new(import_field_checker, export_field_checker, field_normalizer)
    else
      feedme = headers
      current = header
    end
    current_start = s.charpos
    until s.eos?
      case
      when s.scan(/[^<]+/)
        # Ignore junk.
      when s.scan(/[^<]*<(?<field>\w+):(?<length>\d+)(?::(?<type>.))?>/m)
        field, length, type = s[1].downcase, s[2].to_i, s[3] # StringScanner does not support named regexp
        value = s.peek(length)
        s.pos += length
        s.scan(/[^<]*/) # comment
        current[field, type] = value
      when s.scan(/[^<]*<eoh>/i)
        s_charpos = s.charpos
        current.raw = s.string[current_start, s_charpos - current_start]
        current_start = s_charpos
        feedme = records
        current = Record.new(import_field_checker, export_field_checker, field_normalizer)
      when s.scan(/[^<]*<eor>/i)
        s_charpos = s.charpos
        current.raw = s.string[current_start, s_charpos - current_start]
        feedme << current
        current_start = s_charpos
        current = Record.new(import_field_checker, export_field_checker, field_normalizer)
      else
        raise "unexpected string: #{s.string[s.pos..-1]}"
      end
    end

    ADIF_IO.new(header, records)
  end

  # def self.parse_adx(adif)
  #   doc = REXML::Document.new(adif)
  #   header = Header.new
  # 
  #   header_e = doc.get_elements('/ADX/HEADER').first
  #   if header_e
  #     header_e.each_element do |e|
  #       name = e.name
  #       if name == 'USERDEF'
  #         name += e.attribute('FIELDID').value
  #       end
  #       type = e.attribute('TYPE').value rescue nil
  #       header[name.downcase, type] = e.text
  #     end
  #   end
  # 
  #   records = []
  # 
  #   doc.each_element('/ADX/RECORDS/RECORD') do |record_e|
  #     record = Record.new
  #     record_e.each_element do |e|
  #       name = e.name
  #       case name
  #       when 'USERDEF'
  #         name = e.attribute('FIELDNAME').value
  #       when 'APP'
  #         name = "APP_%s_%s" % [ e.attribute('PROGRAMID').value, e.attribute('FIELDNAME')]
  #       end
  #       type = e.attribute('TYPE').value rescue nil
  #       record[name.downcase, type] = e.text
  #     end
  #     records << record
  #   end
  # 
  #   ADIF_IO.new(header, records)
  # end

  class Data
    attr_reader :fields
    attr_accessor :raw

    def initialize(import_field_checker, export_field_checker, field_normalizer, data={})
      # These are shared among all objects concerning one ADI stream.
      # They are mangled by header when a USERDEF_n is read, but not otherwise used.
      # They are used by regular records, to test field validity.
      @import_field_checker = import_field_checker
      @export_field_checker = export_field_checker
      @field_normalizer = field_normalizer

      @fields = {}
      @attributes  = {}

      data.each do |k, v|
        case v
        when Array
          @attributes[k] = v[0]
          @fields[k]  = v[1]
        else
          @fields[k]  = v.to_s
        end
      end
    end

    def [](k)
      @fields[k]
    end
  end

  class Header < Data
    def []=(name, type = nil, value)
      key = name.downcase
      $stderr.puts("Have type #{type} for key #{key}.\n") if type
      @fields[key] = value
      @attributes[key] = type.upcase if type
      if /^userdef\d+$/ =~ key
        puts "Digesting \"#{key}\" with value \"#{value}\" for type #{type}"
        @import_field_checker.add_userdef_checker(type, value)
        @export_field_checker.add_userdef_checker(type, value)
      end
    end
  end

  class Record < Data

    def initialize(import_field_checker, export_field_checker, field_normalizer, data={})
      super
      @import_field_checker = import_field_checker
      @export_field_checker = export_field_checker
      @field_normalizer = field_normalizer
    end

    def adif_ok?
      @fields.reduce(true) do |prev, kv|
        prev ? @export_field_checker.ok?(kv[0], kv[1]) : false
      end
    end

    def adif_ok_lenient?
      @fields.reduce(true) do |prev, kv|
        prev ? @import_field_checker.ok?(kv[0], kv[1]) : false
      end
    end

    def _broken_fields(checker)
      @fields.reduce([]) do |prev, kv|
        unless checker.ok? kv[0], kv[1]
          prev << kv[0]
        end
        prev
      end.sort
    end

    def broken_fields_export
      _broken_fields(@export_field_checker)
    end

    def broken_fields_import
      _broken_fields(@import_field_checker)
    end

    def field_ok_for_output?(field)
      @export_field_checker.ok? field, self[field]
    end

    def field_ok_for_input?(field)
      @import_field_checker.ok? field, self[field]
    end

    def []=(name, type = nil, value)
      key = name.downcase
      @fields[key] = @field_normalizer.normalize(key, value)
      @attributes[key]  = type
    end

    @@length2format = {
      8 => "%Y%m%d",
      13 => "%Y%m%d:%H%M",
      15 => "%Y%m%d:%H%M%S"
    }

    def _datetime(datetimestring)
      format = @@length2format[datetimestring.length]
      DateTime.strptime(datetimestring, format)
    end

    def datetime_on
      _datetime("#{@fields["qso_date"]}:#{@fields["time_on"]}")
    end

    def datetime_off
      if @fields["qso_date_off"]
        _datetime("#{@fields["qso_date_off"]}:#{@fields["time_off"]}")
      else
        _datetime("#{@fields["qso_date"]}:#{@fields["time_off"]}")
      end
    end
  end

  class Writer
    attr_reader :io

    def initialize(io, format = :adi)
      @formatter = (format == :adx ? ADX : ADI).new
      @io = io
      @io << @formatter.start
      @header = true
    end

    def <<(data)
      if @header
        case data
        when Header
          # FIXME: We (may) have the types in attributes...
          data_copy = Header.new(nil, nil, nil, data.fields)
          if data["programid"] and data["programversion"]
            # All is well
          else
            data_copy["programid"] = "adif-io.rb"
            data_copy["programversion"] = ADIF_IO::VERSION
          end
          @io << @formatter.header(data_copy)

        when Record
          self.<< Header.new(nil, nil, nil)
          self.<< data
        else
          @io << data.to_s
        end
        @header = false
      else
        case data
        when Header
          raise "already output records, do not expect Header here."  
        when Record
          @header = false
          @io << @formatter.record(data)
        else
          @io << data.to_s
        end
      end
    end

    def finish
      @io << @formatter.finish
    end

    class Formatter
      def start
        ''
      end

      def header(data)
        ''
      end

      def record(data)
        ''
      end

      def finish
        ''
      end
    end

    class ADI < Formatter
      def start
        "This is an ADIF file generated by Ruby adif-io version #{ADIF_IO::VERSION}.\n\n"
      end

      def header(data)
        data.fields.map {|k, v|
          "<#{k.upcase}:#{v.size}>#{v}"
        }.join("\n") + "<ADIF_VER:5>3.1.0\n<EOH>\n\n"
      end

      def record(data)
        data.fields.map {|k, v|
          "<#{k.upcase}:#{v.size}>#{v}"
        }.join("\n") + "\n<EOR>\n\n"
      end
    end

    class ADX < Formatter
      def start
        %|<?xml version="1.0" encoding="UTF-8"?>\n<ADX>\n|
      end

      def header(data)
        %|<HEADER>\n| +
          data.fields.map {|k, v|
          "<#{k.upcase}>#{xmlescape v}</#{k.upcase}>"
        }.join("\n") +
          %|\n</HEADER>\n<RECORDS>\n|
      end

      def record(data)
        %|<RECORD>\n| +
          data.fields.map {|k, v|
          "<#{k.upcase}>#{xmlescape v}</#{k.upcase}>"
        }.join("\n") +
          %|\n</RECORD>\n|
      end

      def finish
        %|</RECORDS>\n</ADX>|
      end

      XMLSPECIALS = {
        '<' => '&lt;',
        '>' => '&gt;',
        '&' => '&amp;',
        "'" => '&apos;',
        '"' => '&quot;',
      }
      def xmlescape(s)
        s.gsub(/[<>&'"]/, XMLSPECIALS)
      end
    end
  end

  attr_reader :header, :records

  def initialize(header, records)
    @header  = header
    @records = records
  end
end
