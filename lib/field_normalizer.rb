
######################################
# This is generated code!            #
# Do not edit!                       #
# Instead, edit build_code/Rakefile. #
######################################

class FieldNormalizer
  @@nothing = ->(x) {x.to_s}
  @@downcase = ->(x) {x.to_s.downcase}
  @@upcase = ->(x) {x.to_s.upcase}

  @@field2l = Hash.new(@@nothing)
  @@field2l["ant_path"] = @@upcase
  @@field2l["arrl_sect"] = @@upcase
  @@field2l["band"] = @@downcase
  @@field2l["band_rx"] = @@downcase
  @@field2l["call"] = @@upcase
  @@field2l["clublog_qso_upload_status"] = @@upcase
  @@field2l["cnty"] = @@upcase
  @@field2l["cont"] = @@upcase
  @@field2l["darc_dok"] = @@upcase
  @@field2l["dxcc"] = @@upcase
  @@field2l["eqsl_qsl_rcvd"] = @@upcase
  @@field2l["eqsl_qsl_sent"] = @@upcase
  @@field2l["hrdlog_qso_upload_status"] = @@upcase
  @@field2l["lotw_qsl_rcvd"] = @@upcase
  @@field2l["lotw_qsl_sent"] = @@upcase
  @@field2l["mode"] = @@upcase
  @@field2l["my_cnty"] = @@upcase
  @@field2l["my_dxcc"] = @@upcase
  @@field2l["my_state"] = @@upcase
  @@field2l["prop_mode"] = @@upcase
  @@field2l["qrzcom_qso_upload_status"] = @@upcase
  @@field2l["qsl_rcvd"] = @@upcase
  @@field2l["qsl_rcvd_via"] = @@upcase
  @@field2l["qsl_sent"] = @@upcase
  @@field2l["qsl_sent_via"] = @@upcase
  @@field2l["qso_complete"] = @@upcase
  @@field2l["region"] = @@upcase
  @@field2l["state"] = @@upcase

  def normalize(field, value)
    @@field2l[field].call value
  end
end
